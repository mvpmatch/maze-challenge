# Maze API

Implementation of the MVP Match backend challenge.

## Using Postman Collections

Collections use variables for `server` and `port` in urls.

You can define them for example by creating new environment and setting it's values:
```
server: maze.martinsenkerik.cz
port: 80
```