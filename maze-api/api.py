import secrets
from http.client import FORBIDDEN, UNAUTHORIZED, NOT_FOUND, BAD_REQUEST, NOT_ACCEPTABLE, OK

import flask
import flask_login
from flask import abort, request, make_response
from flask_login import login_required, current_user

from model.maze import Maze
from model.user import User
from solver import Solver

app = flask.Flask(__name__)
app.secret_key = "2655e336707782e244090a31cb980aa347b68a89af6d94c81b330a2777829613"
login_manager = flask_login.LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(username):
    return User.get_or_none(User.username == username)


@login_manager.request_loader
def load_user_from_request(request):
    bearer_token = request.headers.get('Authorization')
    if not bearer_token:
        return None
    token = bearer_token.replace('Bearer ', '', 1)
    user = get_user_with_token(token)
    if user:
        return user
    return None


@app.get("/")
def health_check():
    return "", OK


@app.post("/user")
def register():
    user_json = flask.request.json
    if load_user(user_json["username"]):
        abort(FORBIDDEN)
    password_hash = User.hash_password(user_json["password"])
    u = User.create(username=user_json["username"], password=password_hash)
    return "", 201


@app.post("/login")
def login():
    user_json = flask.request.json
    user = get_user_with_credentials(user_json["username"], user_json["password"])
    if not user:
        abort(UNAUTHORIZED)
    user.token = generate_token()
    user.save()
    return {"token": user.token}, 201


@app.post("/maze")
@login_required
def create_maze():
    maze_json = flask.request.json

    try:
        maze = Maze.from_json(maze_json)
    except KeyError or TypeError:
        abort(BAD_REQUEST)
    solver = None
    try:
        solver = Solver(maze)
    except Solver.SolverError as e:
        abort(make_response({"error": str(e)}, BAD_REQUEST))

    maze_error = solver.validate()
    if maze_error:
        print("maze_error = %s" % maze_error)
        abort(make_response(maze_error, BAD_REQUEST))

    maze.user = current_user
    maze.save()
    return maze.to_dict()


@app.get("/maze/<maze_id>")
@login_required
def get_maze(maze_id):
    maze = Maze.get_or_none(Maze.user == current_user, Maze.id == maze_id)
    if not maze:
        abort(NOT_FOUND)
    return maze.to_dict()


@app.get("/maze")
@login_required
def get_mazes():
    return {"mazes": [maze.to_dict() for maze in current_user.mazes]}


@app.get("/maze/<maze_id>/solution")
@login_required
def get_maze_solution(maze_id):
    maze = Maze.get_or_none(Maze.user == current_user, Maze.id == maze_id)
    if not maze:
        abort(NOT_FOUND)

    steps = request.args.get('steps', default="min").lower()
    if steps not in ("min", "max"):
        abort(BAD_REQUEST)
    solver = Solver(maze)
    try:
        path = solver.get_shortest_path() if steps == "min" else solver.get_longest_path()
    except Solver.SolverError:
        abort(NOT_ACCEPTABLE)
    return {"path": path}


def get_user_with_credentials(username, password):
    u = User.get_or_none(User.username == username)
    if not u:
        return None
    if not u.verify_password(password):
        return None
    return u


def get_user_with_token(token):
    return User.get_or_none(User.token == token)


def generate_token():
    return secrets.token_urlsafe(24)
