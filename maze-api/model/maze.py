import peewee

import model
from model.fields import VectorField, WallsField
from model.user import User


class Maze(model.BaseModel):
    id = peewee.AutoField(primary_key=True, unique=True)
    user = peewee.ForeignKeyField(User, backref='mazes', null=False)
    gridSize = VectorField()
    walls = WallsField()
    entrance = peewee.CharField()

    def get_grid_size(self):
        return self.gridSize

    @staticmethod
    def from_json(data):
        print("Maze.from_json(%s)" % data)
        maze = Maze()
        maze.gridSize = VectorField.parse_grid_size(data["gridSize"])
        maze.walls = data["walls"]
        maze.entrance = data["entrance"]
        return maze

    def to_dict(self):
        return {"id": self.id,
                "gridSize": VectorField.encode_grid_size(self.gridSize),
                "walls": self.walls,
                "entrance": self.entrance
                }


with model.database as db:
    db.create_tables([Maze])
