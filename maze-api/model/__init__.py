from peewee import SqliteDatabase, Model

database = SqliteDatabase("db.sqlite", pragmas={'foreign_keys': 1})


class BaseModel(Model):
    class Meta:
        database = database
