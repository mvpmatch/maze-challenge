import peewee


class VectorField(peewee.CharField):
    def db_value(self, value):
        if not value:
            return ""
        db_value = self.encode_grid_size(value)
        return db_value

    def python_value(self, value):
        if not value:
            return []
        return self.parse_grid_size(value)

    @staticmethod
    def parse_grid_size(grid_size: str):
        return grid_size.split("x")

    @staticmethod
    def encode_grid_size(grid_size):
        return "x".join(grid_size)


class WallsField(peewee.TextField):
    def db_value(self, value):
        if not value:
            return ""
        db_value = self.encode_walls_list(value)
        return db_value

    def python_value(self, value):
        if not value:
            return []
        return self.parse_walls_list(value)

    @staticmethod
    def parse_walls_list(walls: str):
        return walls.split(",")

    @staticmethod
    def encode_walls_list(walls):
        return ",".join(walls)
