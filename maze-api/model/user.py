from flask_login import UserMixin
from passlib.hash import bcrypt
from peewee import CharField

import model


class User(model.BaseModel, UserMixin):
    username = CharField(unique=True, primary_key=True)
    password = CharField()
    token = CharField(null=True)

    def get_id(self):
        return self.username

    def verify_password(self, password):
        return bcrypt.verify(password, self.password)

    @staticmethod
    def hash_password(password):
        return bcrypt.hash(password)


with model.database as db:
    db.create_tables([User])
