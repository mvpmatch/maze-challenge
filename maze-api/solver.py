from model.maze import Maze

# indices of the x/y coordinates in a tuple
X = 0  # columns
Y = 1  # rows

FREE = 0
WALL = 1


class Solver:
    class SolverError(RuntimeError):
        pass

    maze = []
    last_path = None

    def __init__(self, maze_data: Maze):
        self.width = int(maze_data.gridSize[X])
        self.height = int(maze_data.gridSize[Y])
        self.entrance = to_coordinates(maze_data.entrance)

        self.init_maze_storage()
        self.fill_with_walls(maze_data.walls)

        self.short_path_finder = PathFinder(self.maze, self.width, self.height, shortest_path=True)
        self.long_path_finder = PathFinder(self.maze, self.width, self.height, shortest_path=False)
        print(self)

    def init_maze_storage(self):
        self.maze = [[0] * self.height for i in range(self.width)]

    def fill_with_walls(self, walls):
        try:
            for wall_code in walls:
                col, row = to_coordinates(wall_code)
                self.maze[col][row] = WALL
        except IndexError:
            raise Solver.SolverError("Walls outside of the maze bounds.")

    def get_longest_path(self):
        return self.get_path(self.long_path_finder)

    def get_shortest_path(self):
        return self.get_path(self.short_path_finder)

    def get_path(self, path_finder):
        self.last_path = path_finder.find_path(self.entrance)
        print(self)
        return [to_cell_code(*coords) for coords in self.last_path]

    def __str__(self):
        lines = []
        for y in range(self.height):
            line = []
            for x in range(self.width):
                if self.last_path and (x, y) in self.last_path:
                    cell = "*"
                else:
                    cell = "#" if self.maze[x][y] else "-"
                line.append(cell)
            lines.append("".join(line))
        return "\n".join(lines)

    def validate(self):
        max_y = self.height - 1
        solutions = 0
        for x in range(self.width):
            if self.maze[x][max_y] == WALL:
                continue
            self.short_path_finder.destination = self.entrance
            path = self.short_path_finder.find_path((x, max_y))
            if len(path) > 0:
                solutions += 1

        if solutions == 1:
            return None
        if solutions == 0:
            return {"error": "There is no path leading from entrance to any point on the bottom row of the maze."}
        if solutions > 1:
            return {"error": "Too many exit points: %d found." % solutions}


class PathFinder:

    def __init__(self, maze, width, height, shortest_path: bool):
        self._destination = None
        self.height = height
        self.width = width
        self.maze = maze

        self.reached_exit = self.reached_last_maze_row

        if shortest_path:
            self.best_length = pow(2, 30)
            self.path_comparator = int.__lt__
        else:
            self.best_length = -1
            self.path_comparator = int.__gt__

    @property
    def destination(self):
        return self._destination

    @destination.setter
    def destination(self, value):
        self._destination = value
        self.update_exit_logic()

    @destination.deleter
    def destination(self):
        self._destination = None
        self.update_exit_logic()

    def update_exit_logic(self):
        if self._destination:
            self.reached_exit = self.reached_destination
        else:
            self.reached_exit = self.reached_last_maze_row

    def find_path(self, start):
        result = []
        if not is_in_bounds(start, (self.width, self.height)):
            return result
        if self.maze[start[X]][start[Y]] == WALL:
            return result
        if self.reached_exit(start):
            return [start]

        # temporarily block the current cell to avoid walking the same cell multiple times
        self.maze[start[X]][start[Y]] = WALL

        next_cells = self.get_surrounding_cells(start)

        # best length is either shortest or longest path depending on the PathFinder configuration
        best_length = self.best_length

        for next_cell in next_cells:
            path = self.find_path(next_cell)
            if len(path) > 0 and self.path_comparator(len(path), best_length) > 0:
                best_length = len(path)
                path.insert(0, start)
                result = path

        # unblock current cell after it has been processed
        self.maze[start[X]][start[Y]] = FREE
        return result

    def get_surrounding_cells(self, coords):
        return [
            (coords[X] + 1, coords[Y]),
            (coords[X], coords[Y] + 1),
            (coords[X] - 1, coords[Y]),
            (coords[X], coords[Y] - 1)
        ]

    def reached_last_maze_row(self, coords):
        return coords[Y] == self.height - 1

    def reached_destination(self, coords):
        return coords == self.destination


def get_row(cell_code):
    return int("".join(filter(str.isdigit, cell_code))) - 1


def get_column_letter(cell_code):
    return "".join(filter(str.isalpha, cell_code))


def get_column_from_letter(column_letter):
    try:
        return ord(column_letter) - ord("A")
    except TypeError:
        raise Solver.SolverError("Invalid column name: %s" % column_letter)


def to_column_letter(col):
    return chr(ord("A") + col)


def to_cell_code(x, y):
    column_letter = to_column_letter(x)
    return column_letter + str(y + 1)


def to_coordinates(cell_code):
    if not cell_code:
        raise Solver.SolverError("Invalid cell: `%s`" % str(cell_code))
    column_letter = get_column_letter(cell_code)
    column = get_column_from_letter(column_letter)
    row = get_row(cell_code)
    return column, row


def is_in_bounds(point, bounds):
    return 0 <= point[X] < bounds[X] and 0 <= point[Y] < bounds[Y]
